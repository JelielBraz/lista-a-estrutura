//Faça um programa para ler 10 números DIFERENTES a serem armazenados em um vetor. Os dados deverão 
//ser armazenados no vetor na ordem que forem sendo lidos, sendo que caso o usuário digite um número que 
//já foi digitado anteriormente, o programa deverá pedir para ele digitar outro número. Exibir na tela o vetor 
//final do que foi digitado. Esse programa deverá ter pelo menos três funções: uma para receber os dados do 
//usuário;  outra  para  verificar  se  o  número  já  se  encontra  armazenado  no  vetor;  outra  para mostrar  os  10 
//números digitados pelo usuário.
#include <stdio.h>
int numeroNaoPresenteNoVetor(int numero, int* vetor, int tamanhoVetor){
    int j;
    for (j=0; j<tamanhoVetor; j++){
        if(numero == vetor[j]){
            return 1;
        }
    }
    return 0;
}

void mostrarNumeros(int* posicaoVetor){
    int i;
    for (i=0; i<10; i++){
        printf("%d - ", *posicaoVetor+i);
    }
}

void recebeNumeros(){
    int i;
    int numeros[10];
    int aux;
    for (i=0; i<10; i++){
        printf("Insira um número: ");
        scanf("%d", &aux);
        while (numeroNaoPresenteNoVetor(aux, numeros, i) == 1)
        {
            printf("Número Já inserido, insira outro valor: ");
            scanf("%d", &aux);
        }
        numeros[i]= aux;
    }
    mostrarNumeros(numeros);
     
}

int main(int argc, char const *argv[])
{
    int posicaoPonteiro;
    recebeNumeros();
    return 0;
}


//vue create app

//na pasta app ---> vue add vuetify