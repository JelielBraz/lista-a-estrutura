// Dado um programa, faça uma função que leia uma matriz de 3 x 3 elementos. Faça outra função que calcule 
// a soma dos elementos que estão na diagonal principal. O resultado da soma deverá ser impresso na função 
//main.
#include <stdio.h>
#include <stdlib.h>


int* lerMatriz(){
    //i para linha; j para coluna
    int i, j, valor;
    
    int *matriz;
    matriz = (int*) malloc(9*sizeof(int));
    int contador =0;
    printf("Você deve inserir uma matriz 3x3\n \n");
    for (i=0; i<3; i++){
        for (j=0;j<3;j++){
            printf("Você esta na linha: %d e na coluna %d \n Agora insira um valor: ", i+1, j+1);
            scanf("%d", &valor);
            matriz[contador] = valor; 
            contador++;
        }
    }
    

    return matriz;
}
int calculaDiagonalPrincipal(int* matriz){
    int i, valor;
    valor = 0;
    for (i=0; i<9; i++){
        if (i==0|| i==4|| i ==8){
            valor += matriz[i];
        }
    }
    free (matriz);
    return valor;
}

int main(int argc, char const *argv[])
{
    int* matriz = lerMatriz();
    int valorDiagonal = calculaDiagonalPrincipal(matriz);
    printf("O valor da soma da diagonal principal é: %d", valorDiagonal);

}
