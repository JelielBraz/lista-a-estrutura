#include <stdio.h>
//Faça um programa que leia um vetor de 5 posições para números  reais e,
// depois, um código inteiro. Se o código  for zero,  finalize o programa; 
// se  for 1, mostre o vetor na ordem direta; se  for 2, mostre o vetor na 
//ordem inversa. Caso, o código for diferente de 0, 1 e 2 escreva uma mensagem
// informando que o código é  inválido. 
void ordenaCorretamente(int* list){
    int i;
    for (i=0; i<5; i++ ){
        printf("%d", list[i]);
    }
}

void ordenaInversamente(int* list){
    int i;
    for (i=4; i>=0; i--){
        printf("%d", list[i]);
    }
}

int main(int argc, char const *argv[])
{
    int numerosReais[5];
    int cod;
    int i;
    for (i=0; i < 5; i++){
        printf("Insira um valor: ");
        scanf("%d", &numerosReais[i]);
    }

    printf("Insira um código. \n Caso o valor for 0: exit \n Caso o valor for 1: Mostrar na ordem. \n Caso o valor for 2: Mostrar na ordem inversa \n Digite o código: ");
    scanf("%d", &cod);

    if (cod < 0 || cod > 2){
        printf("Código Inválido");
        return 1;
    }
    if (cod == 0){
        return 0;
    }
    if (cod == 1){
        ordenaCorretamente(numerosReais);
        return 0;        
    }
    //(só vai chegar aqui se o valor for 2!!!!)
    ordenaInversamente(numerosReais);
    
    
    return 0;
}
