//Escreva um programa para converter uma cadeia de caracteres de letras minúscula em letras maiúsculas. Dica: 
//estude a tabela ASCII. 
#include <stdio.h>
void converte(char* texto){
    int i;
    for (i=0;i<10;i++){
        if ((int)texto[i] >= 'A' && (int)texto[i] <= 'Z'){
            texto[i] = (int)texto[i]+32;
        }else if ((int)texto[i] >= 'a' && (int)texto[i] <= 'z'){
            texto[i] = (int)texto[i]-32;
        }        
    }
}
int main(int argc, char const *argv[])
{
    char texto1[10] = "Jeliel";
    char texto2[10] = "Raphael";
    char texto3[10] = "joelmir";
    char texto4[10] = "joao";
    char texto5[10] = "Hartmann";
    
    converte(texto1);
    converte(texto2);
    converte(texto3);
    converte(texto4);
    converte(texto5);
    
    printf("%s \n", texto1);
    printf("%s \n", texto2);
    printf("%s \n", texto3);
    printf("%s \n", texto4);
    printf("%s \n", texto5);
    
    return 0;
}
