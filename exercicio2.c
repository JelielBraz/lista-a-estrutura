//Faça um programa que leia três valores inteiros e chame uma função que receba estes 3 valores de entrada e
//retorne eles ordenados, ou seja, o menor valor na primeira variável, o segundo menor valor na variável do
//meio, e o maior valor na última variável. Além de ordenar, essa  função deve  retornar o valor 1 se os  três
//valores forem iguais e 0 se existirem valores diferentes. Exibir, na função main, os valores ordenados na tela.

#include <stdio.h>
int ordenado(int *list){
    int x = list[0];
    int y = list[1];
    int z = list[2];

    void inverterVariaveis(int *pa, int *pb);

    if (x > y) inverterVariaveis(&x, &y);
    if (y > z) inverterVariaveis(&y, &z);
    if (x > y) inverterVariaveis(&x, &y);

    list[0] = x;
    list[1] = y;
    list[2] = z;

    if (x == y & y == z)
    {
        return 1;
    }

    return 0;


}

void inverterVariaveis(int *pa, int *pb){
    int temp;
    temp = *pa;
    *pa = *pb;
    *pb = temp;
}

int main(int argc, char const *argv[])
{
    int i;
    int valores[3];
    int retorno;

    for (i=0; i<3; i++){
        printf("Insira um valor: ");
        scanf("%d", &valores[i]);
    }
    retorno = ordenado(valores);

    printf("MENOR %d\n", valores[0]);
    printf("MEIO %d\n", valores[1]);
    printf("MAIOR %d\n", valores[2]);

    printf("Resultado da Comparação %d\n", retorno);

    return 0;
}
