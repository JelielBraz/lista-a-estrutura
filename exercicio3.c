// Implemente uma  função que receba como parâmetro
// um array de números reais de tamanho N e retorne 
// quantos números negativos há nesse array. 
// Essa função deve obedecer ao protótipo: 
// int negativos(float *vet , int n)
#include <stdio.h>


int funcao(float *vet, int n){
    int i;
    int contador = 0;
    for (i=0; i<n; i++ ){
        if (vet[i] < 0){
            contador++;
        }
    }
    return contador;
}

int main(int argc, char const *argv[])
{
    float vet[] = {1,-5,3,-78,5};
    int i = funcao(vet, 5);
    printf("%d", i);
    return 0;
}
