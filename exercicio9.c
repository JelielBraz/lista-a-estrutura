// Faça  um  programa  que  leia  do  usuário  o  tamanho  de  um  vetor  a  ser  
// lido  e  faça  a  alocação  dinâmica  de  memória. Em seguida, leia do usuário
// seus valores e mostre quantos dos números são pares e quantos são 
// ímpares. Antes de terminar o programa, libere a memória alocada. 
#include <stdio.h>
#include <stdlib.h>
void recebeValores(int qtdValores, int* vet){
    int impares, pares, i;
    for (i=0; i<qtdValores; i++){
        printf("Insira um valor no sistema: ");
        scanf("%d", &vet[i]);
        if(vet[i]%2 ==0){
            pares++;
        }else{
            impares++;
        }
        
    }
    printf("Par: %d \n Impar: %d", pares, impares);
}

void lerDados(){
    int qtdValores;
    printf("Quantos dados você deseja adicionar no sistema? ");
    scanf("%d", &qtdValores);

    int *variavel;
    variavel =(int*) malloc(qtdValores*sizeof(int));
    recebeValores(qtdValores, variavel);
    free(variavel);
}

int main(int argc, char const *argv[])
{
    lerDados();/* code */
    return 0;
}