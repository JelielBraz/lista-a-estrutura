//Faça um programa que receba uma palavra e calcule quantas vogais possui essa palavra. 
#include <stdio.h>
int contadorDeVogais(char texto[]){
    int i;
    int contador = 0;
    for (i=0; i<10; i++){
        if ((texto[i]== 'a')
            ||(texto[i]=='A')
            ||(texto[i]=='e')
            ||(texto[i]=='E')
            ||(texto[i]=='i')
            ||(texto[i]=='I')
            ||(texto[i]=='o')
            ||(texto[i]=='O')
            ||(texto[i]=='u')
            ||(texto[i]=='U') ){
            contador ++;
        }
    }
    return contador;
}


int main(int argc, char const *argv[])
{
    char texto1[10] = "jeliel";
    char texto2[10] = "Raphael";
    char texto3[10] = "joelmir";
    char texto4[10] = "joao";
    char texto5[10] = "Hartmann";
    
    int palavras_texto1 = (contadorDeVogais(texto1));
    int palavras_texto2 = (contadorDeVogais(texto2));
    int palavras_texto3 = (contadorDeVogais(texto3));
    int palavras_texto4 = (contadorDeVogais(texto4));
    int palavras_texto5 = (contadorDeVogais(texto5));
    
    printf("%d - ", palavras_texto1);
    printf("%d - ", palavras_texto2);
    printf("%d - ", palavras_texto3);
    printf("%d - ", palavras_texto4);
    printf("%d - ", palavras_texto5);

    return 0;
}
