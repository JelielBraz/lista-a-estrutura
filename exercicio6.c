#include <stdio.h>
#include <math.h>
//Faça um programa que calcule o desvio padrão de um vetor v contendo
//n = 10 números, onde m é a média do vetor.

int main()
{
    int numeros[10];
    int i;

    printf("Digite 10 numeros: ");

    for(i=0; i < 10; ++i) scanf("%d", &numeros[i]);

    void calcularDesvio(int listaNumeros[]);

    calcularDesvio(numeros);

    return 0;
}

void calcularDesvio(int listaNumeros[]){

    float soma, media, potencializacao, desvio;

    int i;

    for (i = 0; i < 10; i++) soma += listaNumeros[i];

    media = soma/10; // Linhas 28-30 cálculo da média

    for (i = 0; i < 10; i++) potencializacao += pow(listaNumeros[i] - media, 2); // Cálculo do quadrado da distância entre cada ponto e a média

    desvio = sqrt(potencializacao/10); // Raiz quadrada da divisão da etapa anterior pelo numero de pontos

    printf("\nDesvio Padrão = %.4f", desvio);

}